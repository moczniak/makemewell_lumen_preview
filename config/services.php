<?php
return [
    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ]
];
