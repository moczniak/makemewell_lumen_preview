<?php

namespace App\Http\Middleware;

use Closure;
use GenTux\Jwt\GetsJwtToken;


class MyJwtAuth
{
  use GetsJwtToken;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->jwtToken()->validate()) {
          return $next($request);
        }else {
          return response('Unauthorized.', 401);
        }
    }
}
