<?php
namespace App\Http\Models\Main;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

use App\Http\Models\Main\MembersForgetPsw;

class MemberMain extends Model {
    public $timestamps = false;
    protected $table = 'members';

    protected $fillable = ['uid', 'login','facebook_id','password', 'email', 'activate', 'reg_date', 't1_uid'];
    protected $primaryKey = 'uid';


    protected $connection = 'mmwMain';

    public static function blowfishCrypt($password,$cost)  {
      $chars='./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      $salt=sprintf('$2a$%02d$',$cost);
      for($i=0;$i<22;$i++) $salt.=$chars[rand(0,63)];
      return crypt($password,$salt);
    }

    public function forgetPsw() {
      return $this->hasOne('App\Http\Models\Main\MembersForgetPsw', 'uid', 'uid');
    }
    public static function getInstanceByEmail($email) {
      return MemberMain::where('email', $email)->first();
    }

    public static function createNewUser($request) {
      $member = new MemberMain();
      $member->login = $request->get('username');
      $member->password = MemberMain::blowfishCrypt($request->get('password'), 10);
      $member->email = $request->get('email');
      $member->activate = 1;
      $member->reg_date = time();
      $member->save();
    }

    public static function signInUser($username, $password) {
      $member = MemberMain::where('login', $username)->first();
      if (crypt($password, $member->password) === $member->password) {
        return $member;
      }else {
        throw new \Exception ('Hasło niepoprawne');
      }
    }

    public static function sendEmailWithLink($memberInstance) {
      $token = str_random(100);
      $text = 'Twój login to: '.$memberInstance->login."\r\n".'Kliknij w linka:'."\r\n".'http://'.env('DOMAIN').'/forgetPassword/'.$token;
      Mail::raw($text, function ($message) use ($memberInstance) {
        $message->subject('Przypomnienie hasła MMW');
        $message->from(env('ADMIN_EMAIL'), 'Moczniak MMW');
        $message->to($memberInstance->email);
      });
      $memberForgetPsw = new MembersForgetPsw();
      $memberForgetPsw->uid = $memberInstance->uid;
      $memberForgetPsw->hash = $token;
      $memberInstance->forgetPsw()->save($memberForgetPsw);

    }

    public static function changePasswordByInstance($member, $password) {
      $member->password = MemberMain::blowfishCrypt($password, 10);
      $member->save();
    }



}
