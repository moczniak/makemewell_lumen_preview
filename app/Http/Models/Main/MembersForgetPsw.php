<?php
namespace App\Http\Models\Main;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;


class MembersForgetPsw extends Model {
    public $timestamps = false;
    protected $table = 'members_forget_psw';

    protected $fillable = ['mfp', 'uid','hash'];
    protected $primaryKey = 'mfp';


    protected $connection = 'mmwMain';

    public function member() {
      return $this->belongsTo('App\Http\Models\Main\MemberMain', 'uid', 'uid');
    }

    public static function clearTokens($uid) {
      MembersForgetPsw::where('uid', $uid)->delete();
    }

    public static function getInstanceByToken($token) {
      return MembersForgetPsw::where('hash', $token)->first();
    }

}
