<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class MemberSkill extends Model {
    public $timestamps = false;
    protected $table = 'members_skills';

    protected $fillable = ['skillID', 'uid', 'sid1', 'sid2', 'sid3', 'sid4', 'sid5',
                           'dildo', 'gumy', 'kajdanki', 'kulki', 'pejcze', 'sznury', 'wibratory'];
    protected $primaryKey = 'skillID';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }

}
