<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;
use App\Http\Classes\SocketEmitter;

class Chat extends Model {
    public $timestamps = false;
    protected $table = 'klachy';

    protected $fillable = ['post_id', 'uid', 'login', 'room', 'tekst', 'post_date'];
    protected $primaryKey = 'post_id';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }


    public static function getRoomsList(int $userID): array {
      $rooms = [
        ['roomID' => 0, 'name'=> 'Główny', 'messages' => array_reverse(Chat::where('room', 0)->take(100)->orderBy('post_date', 'desc')->get()->toArray())],
        ['roomID' => 1, 'name'=> 'Handel', 'messages' => array_reverse(Chat::where('room', 1)->take(100)->orderBy('post_date', 'desc')->get()->toArray())]
      ];
      $clan = Clan::getClanIDByUserIDWithMessages($userID);
      if ($clan->roomID === 0) { // FIXME : przy tworzeniu klanu dodaj automatyczne przydzielanie roomID i skasuj tutaj ustawianie
        $chatLastActiveID = Chat::orderBy('room', 'desc')->first();
        $chatLastActiveID->room = $chatLastActiveID->room < 2 ? 1 : $chatLastActiveID->room;
        $clan->roomID = $chatLastActiveID->room +1;
        $clan->save();
      }
      if (!is_null($clan)) {
        $rooms[] = ['roomID' => $clan->roomID, 'name'=> $clan->nazwa, 'messages'=> array_reverse($clan->chatMessage->toArray())];
      }
      // TODO: jak zrobie orgie to dodac pokoj czatowy orgii
      return $rooms;
    }


    public static function roomPermission(int $userID, int $roomID): bool {
      $permission = false;
      if ($roomID == 0 || $roomID == 1) {
        $permission = true;
      }else {
        $clan = Clan::getClanIDByUserID($userID);
        if (!is_null($clan)) {
          if ($clan->roomID == $roomID) {
            $permission = true;
          }
        }
        if ($permission == false) {
          // TODO : chat orgii permision
        }
      }
      return $permission;
    }

    public static function newMessage(int $userID, string $username,  int $roomID, string $message) {
      if(empty(trim($message))) {
        throw new \Exception ('Pusta wiadomość! tak się bawić nie będziemy!!');
      }
      if (Chat::roomPermission($userID, $roomID)) {
        $chat = new Chat();
        $chat->uid = $userID;
        $chat->login = $username;
        $chat->tekst = $message;
        $chat->post_date = time();
        $chat->room = $roomID;
        $chat->save();
        SocketEmitter::emitNewChatMessage($chat->toArray());
      }else {
        throw new \Exception ('Nie oszukuj! nie masz dostępu do tego pokoiku dziwko!!');
      }
    }



}
