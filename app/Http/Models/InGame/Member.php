<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\InGame\MembersStat;
use App\Http\Models\InGame\MembersSkill;
use App\Http\Models\InGame\MembersSetting;
use App\Http\Models\InGame\MsgList;
use App\Http\Models\InGame\MsgTxt;


class Member extends Model {
    public $timestamps = false;
    protected $table = 'members';

    protected $fillable = ['uid', 'reff','reffDone','login', 'tired', 'hajs', 'vip', 'vipNotMove',
                           'slawa', 'wpoint', 'hiddenPKT', 'bilansKonfro', 'chcica', 'chcica_stamp',
                           'prestiz', 'soczki', 'ciaza', 'syf', 'dupsko', 'okres', 'dziewice',
                           'avatar_img_path', 'top_anime', 'menu_anime', 'zeton_earn', 'zeton_buy',
                           'tutorial', 'newTutorial', 'burdelCheckTime'];
    protected $primaryKey = 'uid';

    public function items() {
      return $this->hasMany('App\Http\Models\InGame\MemberItem', 'uid', 'uid');
    }
    public function setting() {
      return $this->hasOne('App\Http\Models\InGame\MemberSetting', 'uid', 'uid');
    }
    public function skills() {
      return $this->hasOne('App\Http\Models\InGame\MemberSkill', 'uid', 'uid');
    }
    public function stat() {
      return $this->hasOne('App\Http\Models\InGame\MemberStatistic', 'uid', 'uid');
    }


    public static function getPrestigeFactors(float $prestige) {
      $defaultFactor = 12;
      $prestigeLevels = [4500000, 3500000, 2500000, 1000000, 500000, 200000, ];
      $prestigeFactorPerLevel = [800, 400, 200, 100, 50, 20];

      foreach ($prestigeLevels as $index => $prestigeLevel) {
        if ($prestige > $prestigeLevel) {
          return $prestigeFactorPerLevel[$index];
        }
      }
      return $defaultFactor;
    }

    public static function getWithStatistic(int $userID): Member {
      return Member::where('uid', $userID)->with('stat')->first();
    }

    public static function getWithSettings(int $userID): Member {
      return Member::where('uid', $userID)->with('setting')->first();
    }

    public static function getMemberByUserID(int $userID): Member {
      return Member::find($userID);
    }

    public static function getWithSettingsAndSkills(int $userID): Member {
      return Member::where('uid', $userID)->with('setting')->with('skills')->first();
    }

    public static function getMemberPrestigeByUserID(int $userID): float {
      $member = Member::find($userID);
      return $member->prestiz;
    }

    public static function prepareInfo(Member $user): array {
      return [
        'money' => $user->hajs,
        'prestige' => $user->prestiz,
        'vip' => $user->vip,
        'fame' => $user->slawa
      ];
    }

    public static function getLocationPlusMinutes(Member $user): int {
      $minutes = 0;
      if (time() <= $user->syf) {
        $minutes += 5;
      }
      if (time() <= $user->dupsko) {
        $minutes += 6;
      }
      if (!empty($user->ciaza)) {
        $pregnancyData = explode(';', $user->okres);
        if (time() <= $pregnancyData[1] && $pregnancyData[0] == 1) {
          $minutes += 8;
        }
      }
      if (!empty($user->okres)) {
          $periodData = explode(';', $user->okres);
          if (time() >= $periodData[0] && time() <= $periodData[1]) {
            $minutes += 4;
          }
      }
      if ($user->chcica < 3) {
        $minutes += 10;
      }
      return $minutes;
    }

    public static function prepareHealthInfo(Member $user): array {
      $period = false;
      $pregnancy = false;
      $disease = false;
      $user = Member::updatePeriod($user, false);
      $user = Member::updatePregnancy($user, false);
      $user->save();
      if (!empty($user->okres)) {
          $periodData = explode(';', $user->okres);
          $period = time() >= $periodData[0] && time() <= $periodData[1] ? true : false;
      }
      if (!empty($user->ciaza)) {
        $pregnancyData = explode(';', $user->ciaza);
        $pregnancy = time() <= $pregnancyData[1] && $pregnancyData[0] == 1 ? true : false;
      }
      $disease = time() <= $user->syf ? true : false;

      return [
        'desire' => $user->chcica,
        'period' => $period,
        'pregnancy' => $pregnancy,
        'disease' => $disease
      ];
    }

    public static function updatePeriod(Member $user, $save = true) {
      if (empty($user->okres)) {
    		$firstDay = date('Y-m') . '-' . random_int(1,25);
    		$firstDay = strtotime($firstDay) + (31 * 24 * 60 * 60) + random_int(360,82800);
    		$threeDay = $firstDay + (2 * 24 * 60 * 60) + (12 * 60 * 60) + random_int(360,43200);
        $user->okres = $firstDay . ';' . $threeDay;
    	}else {
    		$periodData = explode(';', $user->okres);
    		if ($periodData[1] < time()) {
    			$periodFirst = $periodData[0] + (random_int(21, 31) * 24 * 60 * 60) + random_int(-82800, 82800);
    			$periodEnd = $periodFirst + (2 * 24 * 60 * 60) + (12 * 60 * 60) + random_int(360, 43200);
          $user->okres = $periodFirst.";".$periodEnd;
    		}
    	}
      if ($save) {
        $user->save();
      }else {
        return $user;
      }
    }

    public static function updatePregnancy(Member $user, $save = true) {
      if (!empty($user->ciaza)) {
        $pregnancyData = explode(';', $user->ciaza);
        $sell = false;
      	if ($pregnancyData[0] == 2 && time() >= $pregnancyData[1]) {
          $user->ciaza = '';
      	}
      	if ($pregnancyData[0] == 1 && time() >= $pregnancyData[1]) {
          $babyChanceForSell = 50;
      		if (random_int(1, 100) <= $babyChanceForSell) {
      			if (random_int(1, 100) <= 10) {
      				$sell = true;
      			}
      		}
      		if ($sell) {
            $user->load('stat');
            $bestLocation = Location::getBestLocationAvailable($user);
        		$cost = $user->hajs = 7.5 * (1 +($user->stat->work_sec / 60 / 60 / 100)) * ($bestLocation->wsp + 0.35) * 20;
            $user->hajs += $cost;
            Raport::newBabySellSuccessRaport($user->uid, $pregnancyData[1], $cost);
      		}else {
            Raport::newBabySellFailedRaport($user->uid, $pregnancyData[1], $cost);
      		}

      		$user->setting->bez_gumy = 0;
          $user->setting->antyplod_hour = 12;
          $user->setting->antyplod_timeset = 0;
          $user->setting->save();
          $user->ciaza = '';
      	}
      }
      if ($save) {
        $user->save();
      }else {
        return $user;
      }
    }






    public static function createWorld_T1($userInstance) {
      $stat = new MembersStat();
      $skills = new MembersSkill();
      $skills->sid1 = 5;
      $skills->sid2 = 5;
      $skills->sid3 = 5;
      $skills->sid4 = 5;
      $skills->sid5 = 5;
      $skills->dildo = 5;
      $skills->gumy = 5;
      $skills->kajdanki = 5;
      $skills->kulki = 5;
      $skills->pejcze = 5;
      $skills->sznury = 5;
      $skills->wibratory = 5;
      $settings = new MembersSetting();
      $settings->bar_hour = 1;
      $settings->latarnia_minute = 30;
      $settings->kurs_amount = 1;
      $member = new Member();
      $member->login = $userInstance->login;
      $member->hajs = 250;
      $member->slawa = 10;
      $member->chcica = 100;
      $member->chcica_stamp = time();
      $member->zeton_earn = 100000;
      $member->prestiz = 5;
      $member->save();
      $member->skills()->save($skills);
      $member->stat()->save($stat);
      $member->setting()->save($settings);
      $userInstance->t1_uid = $member->uid;
      $userInstance->save();
      $tytul = 'Witamy w MakeMeWell!';
      $tekst = 'Witaj dzielna podróżniczko szukająca rozkoszy ekstazy i być może miłości! Niestety trafiłaś do trudnego zawodu w którym nie ma lekko, musisz odbywać ciągle podróże pod latarnie i obłusigiwać wielu klientów na dzień, dam ci rade! by to zrobić wejdź w dom i obrazki które zobaczysz przemieśc na prawą strone, i zapisz grafik! :)
      <br><br>
      Jeśli jesteś ciekawa co oferuje ten świat, zajrzyj na nasze forum! link znajdziesz w stopce lub pod menu, dowiesz sie tam być może przydatnych informacji lub poznasz ludzi z którymi stworzysz wspólnote kobiet pracujących zwanych klanem!
      <br><br>
      Jeśli nie aktywowałaś jeszcze konta, to zrób bo to jak najszybciej, bez tego masz zablokowane niektóre części gry, jak choćby bar gdzie twoje koleżanki po fachu toczą żywe rozmowy o życiu i śmierci!
      <br><Br>
      _____<br>
      Twój Szef! :)';
      $lastMID = MsgList::select('mid')->orderBy('mid', 'desc')->take(1)->get();
      $msgList = new MsgList();
      $msgList->uid = $member->uid;
      $msgList->tytul = $tytul;
      $msgList->last_post = time();
      $msgList->mid = $lastMID[0]->mid+1;
      $msgList->save();
      $msgTxt = new MsgTxt();
      $msgTxt->tekst = $tekst;
      $msgTxt->post_date = time();
      $msgTxt->mid = $lastMID[0]->mid+1;
      $msgTxt->save();
      return $userInstance;
    }

}
