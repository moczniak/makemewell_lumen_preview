<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

use App\Http\Classes\WhoreJob;


class Shedule extends Model {
    public $timestamps = false;
    protected $table = 'grafikNew';

    protected $fillable = ['gid', 'uid', 'typ','location','locTimeLimit', 'start_time', 'end_time', 'stamp',
                           'option', 'start'];
    protected $primaryKey = 'gid';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }
    public function locationData() {
      return $this->belongsTo('App\Http\Models\InGame\Location', 'location', 'code');
    }

    public function updateShedule(Member $user, array $vipStatus): array {
      $activeShedule = Shedule::where('uid', $user->uid)->where('start', 1)->first();
      $locationPlusMinutes = Member::getLocationPlusMinutes($user);
      if (!is_null($activeShedule)) {
        switch ($activeShedule->typ) {
          case 'latarnia':
            $this->locationJob($user, $vipStatus, $activeShedule, $locationPlusMinutes);
          break;
          case 'odpoczynek':
            $this->restJob($user, $vipStatus);
          break;
          case 'pvp':
            $this->pvpJob($user, $activeShedule);
          break;
          case 'bar':
            $this->barJob($user, $vipStatus, $activeShedule);
          break;
        }
        if ($activeShedule->end_time <= time()) {
          $this->setNewActiveShedule($user, $vipStatus, $activeShedule);
        }else {
          return ['nextStamp' => $activeShedule->stamp,
                  'refferStamp' => $this->getRefferStamp($vipStatus, $activeShedule),
                  'job'=> true,'typ'=> $activeShedule->typ,
                  'fasterComeBackFactor' => $vipStatus['fasterComeBack'] ? Vip::$factors['fasterComeBack'] : 1,
                  'endStamp' => $activeShedule->end_time,
                  'locationPlusMinutes' => $locationPlusMinutes
                 ];
        }
      }else {
        $this->restJob($user, $vipStatus, 4);
      }
      $user->save();
      return ['job'=> false];
    }
    private function getRefferStamp(array $vipStatus, Shedule $activeShedule): int {
      $jobSeconds = ($activeShedule->option * 8 * 60);
      $jobSeconds = $vipStatus['fasterComeBack'] ? $jobSeconds * Vip::$factors['fasterComeBack'] : $jobSeconds;
      return $activeShedule->stamp - $jobSeconds;
    }

    private function locationJob(Member $user, array $vipStatus, Shedule $activeShedule, int $locationPlusMinutes) {
      $this->checkStampFinish($user, $vipStatus, $activeShedule);
      if ($user->setting->alfons_mode == 1) {
        if ($activeShedule->stamp <= time()) {
          if ($activeShedule->stamp <= $activeShedule->end_time) {
            $activeShedule->stamp = $activeShedule->stamp == 0 ? $activeShedule->start_time : $activeShedule->stamp;
            $activeShedule->stamp = $activeShedule->stamp < $user->setting->alfons_stamp ? $user->setting->alfons_stamp : $activeShedule->stamp;
            $randomOption = random_int(1, 3);
            $jobSeconds = ($randomOption * 8 * 60);
            $jobSeconds = $vipStatus['fasterComeBack'] ? $jobSeconds * Vip::$factors['fasterComeBack'] : $jobSeconds;
            $jobSeconds += ($locationPlusMinutes * 60);
            $newStamp = $jobSeconds + $activeShedule->stamp;
            if ($newStamp > $activeShedule->end_time) {
              try {
                $tmpArray = $this->fitNewStamp($vipStatus, $activeShedule, $locationPlusMinutes);
                $newStamp = $tmpArray['stamp'];
                $randomOption = $tmpArray['option'];
              }catch(\Exception $e) { }
            }
            $activeShedule->stamp = $newStamp;
            $activeShedule->option = $randomOption;
            $this->locationJob($user, $vipStatus, $activeShedule, $locationPlusMinutes);
          }else {
            $this->setNewActiveShedule($user, $vipStatus, $activeShedule);
          }
        }else {
          $activeShedule->save();
        }
      }
    }
    private function checkStampFinish(Member $user, array $vipStatus, Shedule $activeShedule) {
      if ($activeShedule->stamp <= time() && $activeShedule->stamp != 0 && $activeShedule->option > 0) {
        $job = new WhoreJob($user, $vipStatus, $activeShedule);
        $job->main();
        Raport::newLocationJobRaport($job->raportData);
      }
    }

    private function fitNewStamp(array $vipStatus, Shedule $activeShedule, int $locationPlusMinutes): array {
      for ($i = 3; $i > 0; $i--) {
        $jobSeconds = ($i * 8 * 60);
        $jobSeconds = $vipStatus['fasterComeBack'] ? $jobSeconds * Vip::$factors['fasterComeBack'] : $jobSeconds;
        $jobSeconds += ($locationPlusMinutes * 60);
        $newStamp = $jobSeconds + $activeShedule->stamp;
        if ($newStamp < $activeShedule->end_time) {
          return [
            'stamp' => $newStamp,
            'option' => $i
          ];
        }
      }
      throw new \Exception ('Cannot find new stamp');
    }

    private function setNewActiveShedule(Member $user, array $vipStatus, Shedule $activeShedule) {
      $activeShedule->delete();
      Shedule::where('uid', 1)->where('start', 0 )->orderBy('start_time', 'asc')->take(1)->update(['start' => 1]);
      $this->updateShedule($user, $vipStatus);
    }

    private function restJob(Member $user, array $vipStatus, int $hour = 3) {
      $regenerationSeconds = $hour * 60 * 60;
      $regenerationSeconds *= $vipStatus['fasterChcicaReg'] ? Vip::$factors['fasterChcicaReg'] : 1;
      $timePassed = time() - $user->chcica_stamp;
      $onePerSeconds =  $regenerationSeconds / 100;
      $diffrence = intval($timePassed / $onePerSeconds);
      if ($diffrence > 0) {
        $missing = 100 - $user->chcica;
        $diffrence = $diffrence > $missing ? $missing : $diffrence;
        $user->chcica_stamp += $diffrence * $onePerSeconds;
        $user->chcica += $diffrence;
        $user->chcica = $user->chcica > 100 ? 100 : $user->chcica;
      }
    }
    private function barJob(Member $user, array $vipStatus, Shedule $activeShedule) {
      // FIXME: cos tu nie dziala bo nie dodalo raportu!!
      if ($activeShedule->end_time <= time()) {
        $timePassed = ($activeShedule->end_time - $activeShedule->start_time) / 60 / 60;
        $user->load('stat');
        $userStat = $user->stat;
        $barPosition = BarPosition::getUserPosition($userStat);
        $money = $timePassed * $barPosition['hourMoney'];
        $money *= $vipStatus['moreHajsKlient'] ? Vip::$factors['moreHajsKlient'] : 1;
        if ($money > 0) {
          $paymentID = BarPayment::newPayment($user->uid, $money);
          Raport::newBarRaport($user->uid, $money, $activeShedule->end_time, $paymentID);
          $userStat->bar_sec += ($timePassed * 60 * 60);
          $userStat->bar_byl += 1;
          $userStat->save();
        }
      }
    }
    private function pvpJob(Member $user, Shedule $activeShedule) {
      if ($activeShedule->stamp != 0 && $activeShedule->stamp <= time()) {
        // TODO : dodac pvp
      }
    }


    public static function setOptionAndStampForActiveShedule(Member $user, array $vipStatus, int $option) {
      if ($option <= 0 && $option > 3) {
        throw new \Exception ('Nie czituj szmato');
      }
      $activeShedule = Shedule::where('uid', $user->uid)->where('start', 1)->first();
      if (!is_null($activeShedule)) {
        if ($activeShedule->typ == 'latarnia') {
          $activeShedule->stamp = time();
          $jobSeconds = ($option * 8 * 60);
          $jobSeconds = $vipStatus['fasterComeBack'] ? $jobSeconds * Vip::$factors['fasterComeBack'] : $jobSeconds;
          $jobSeconds += (Member::getLocationPlusMinutes($user) * 60);
          $newStamp = $jobSeconds + $activeShedule->stamp;
          if ($newStamp > $activeShedule->end_time) {
            throw new \Exception ('Tego nie da rady bejbe, za późno już');
          }
          $activeShedule->stamp = $newStamp;
          $activeShedule->option = $option;
          $activeShedule->save();
        }
      }
    }

    public static function saveShedule(Member $user, array $vipStatus, array $locations): array {
      $pass = NULL;
      $grafikLocation = NULL;
      $typ = NULL;
      $jobMinutes = NULL;
      $perm = NULL;
      $minutesSum = 0;
      $endTime = NULL;
      $startTime = NULL;
      $start = NULL;
      $newActiveShedule = NULL;

      $activeShedule = Shedule::where('uid', $user->uid)->where('start', 1)->first();
      if ($vipStatus['big_grafik']) {
        if (count($locations) + (is_null($activeShedule) ? 0 : 1) > 6) {
          throw new \Exception ('Przekroczyłaś limit kafelek szmato');
        }
      }else {
        if (count($locations) + (is_null($activeShedule) ? 0 : 1) > 2) {
          throw new \Exception ('Przekroczyłaś limit kafelek szmato');
        }
      }

      Shedule::where('uid', $user->uid)->where('start', 0)->delete();
      $specialLong = Location::select('code', 'specialLong')->where('specialLong', '>', 0)->get();
      $locationsDetails = Location::whereIn('code', $locations)->with(['locationUnlock' => function($query) use ($user){
        $query->where('uid', $user->uid);
      }])->get()->toArray();


      foreach ($locations as $index => $location) {
        $grafikLocation = $location;
        $pass = false;
        try {
          $pass = $activeShedule->location != $location ? true : false;
          $endTime = $activeShedule->end_time;
          $startTime = 1;
        }catch(\Exception $e) {
          $pass = true;
        }finally {
          if ($pass) {
            $typ = 'latarnia';
  					$jobMinutes = $user->setting->latarnia_minute;
            if (!empty($specialLong)) {
  							foreach($specialLong as $special)
  							{
  								if($location == $special->code) {
  									$jobMinutes *= $special->specialLong;
  									break;
  								}
  							}
  					}
            switch($location) {
              case 'odpoczynek':
                $typ = $location;
                $jobMinutes = $vipStatus['fasterChcicaReg'] ? (3 * 60 * Vip::$factors['fasterChcicaReg']) : (3 * 60);
              break;
              case 'pvp':
                $typ = "pvp";
    						$grafikLocation = "";
    						$jobMinutes = 30;
              break;
              case 'orgia':
                $typ = "orgia";
    						$jobMinutes = 90;
              break;
              case 'bar':
                $typ = $location;
    						$jobMinutes = $user->setting->bar_hour * 60;
              break;
            }
            $perm = true;
            if ($typ == 'latarnia') {
              $key = array_search($location, array_column($locationsDetails, 'code'));
  						if ($locationsDetails[$key]['unlock'] == 0) {
  							if($user->prestiz < $locationsDetails[$key]['prestizn']) {
  								$perm = false;
  							}
  						}else {
  							if (empty($locationsDetails[$key]['locationUnlock'])) {
  								$perm = false;
  							}
  						}
            }
            if ($perm) {
              $startTime = is_null($startTime)? time() : $endTime;
    					$endTime = $startTime + ($jobMinutes * 60);
              $start = $index == 0 ? 1 : 0;
              $start = is_null($activeShedule) ? $start : 0;
              $shedule = new Shedule();
              $shedule->uid = $user->uid;
              $shedule->typ = $typ;
              $shedule->location = $grafikLocation;
              $shedule->locTimeLimit = $jobMinutes;
              $shedule->start = $start;
              $shedule->start_time = $startTime;
              $shedule->end_time = $endTime;
              $shedule->save();
              if ($shedule->start == 1) {
                $newActiveShedule = $shedule;
              }

            }
          }
        }
      }
      return [
        'activeEndTime' => !is_null($endTime)? $endTime : $activeShedule->end_time,
        'activeLocation' => is_null($activeShedule) ? $newActiveShedule->location : $activeShedule->location
      ];
    }


}
