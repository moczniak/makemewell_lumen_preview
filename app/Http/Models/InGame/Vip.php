<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class Vip extends Model {
    public $timestamps = false;
    protected $table = 'vip';

    protected $fillable = ['vid', 'uid', 'usluga', 'start_time', 'end_time', 'licznik'];
    protected $primaryKey = 'vid';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }

    public static $slotsPoints  = [
      ['cost' => 420, 'amount' => 50],
      ['cost' => 180, 'amount' => 25],
      ['cost' => 90, 'amount' => 15],
      ['cost' => 40, 'amount' => 7],
      ['cost' => 10, 'amount' => 3]
    ];

    public static $services = [
      'pvpInstant' => false,
      'pvpLock' => false,
      'burdelPremiumView' => false,
      'fasterComeBack' => false,
      'fasterChcicaReg' => false,
      'moreHajsKlient' => false,
      'Burdel5Kolejka' => false,
      'big_grafik' => false,
      'big_kurs' => false,
      'chcicaReg' => false
    ];

    public static $factors = [
      'fasterComeBack' => 0.8,
      'fasterChcicaReg' => 0.8,
      'moreHajsKlient' => 1.2
    ];

    public static function getServicesByUserID(int $userID): array {
      $servicesStatus = Vip::$services;
      $services = Vip::where('uid', $userID)->where(function($query) {
        $query->where('end_time', '<=', time())->orWhere('licznik', '>', 0);
      })->get()->toArray();
      foreach ($services as $service) {
          if (array_key_exists($service['usluga'], $servicesStatus)) {
            if ($service['usluga'] == 'pvpInstant') {
              $servicesStatus[$service['usluga']] = $service['licznik'] > 0 ? true : false;
            }else {
              $servicesStatus[$service['usluga']] = true;
            }
          }
      }
      return $servicesStatus;
    }
}
