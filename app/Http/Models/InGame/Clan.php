<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class Clan extends Model {
    public $timestamps = false;
    protected $table = 'klan';

    protected $fillable = ['kid', 'roomID', 'nazwa', 'skrot', 'opis', 'skarbiec', 'slawa', 'podatek'];
    protected $primaryKey = 'kid';

    public function clanMember() {
      return $this->hasMany('App\Http\Models\InGame\ClanMember', 'kid', 'kid');
    }

    public function chatMessage() {
      return $this->hasMany('App\Http\Models\InGame\Chat', 'room', 'roomID');
    }

    public static function getClanIDByUserID(int $userID): Clan {
      return Clan::with(['clanMember' => function($query) use ($userID){
        $query->where('uid', $userID);
      }])->whereHas('clanMember', function($query) use ($userID){
        $query->where('uid', $userID);
      })->first();
    }

    public static function getClanIDByUserIDWithMessages(int $userID): Clan {
      return Clan::with(['clanMember' => function($query) use ($userID){
        $query->where('uid', $userID);
      }, 'chatMessage' => function($query){
        $query->orderBy('post_date', 'desc')->take(100);
      }])->whereHas('clanMember', function($query) use ($userID){
        $query->where('uid', $userID);
      })->first();
    }


}
