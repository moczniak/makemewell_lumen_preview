<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class MsgList extends Model {
    public $timestamps = false;
    protected $table = 'msg_list';

    protected $fillable = ['listID', 'mid', 'uid','rozmowca','mores', 'tytul', 'read', 'last_post', 'radaNadzorcza'];
    protected $primaryKey = 'listID';

    public function msgTxt() {
      return $this->hasMany('App\Http\Models\InGame\MsgTxt', 'mid', 'mid');
    }

}
