<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model {
    public $timestamps = false;
    protected $table = 'osiagniecia';

    protected $fillable = ['osid', 'nazwa', 'level', 'typ', 'wym', 'txt'];
    protected $primaryKey = 'osid';

    public function achievementComplete() {
      return $this->belongsTo('App\Http\Models\InGame\AchievementComplete', 'osid', 'osid');
    }

    public static $achievementVipRewards  = [
      ['name' => 'Dzika Kurwa Sukcesu', 'vipAmount' => 50],
      ['name' => 'Dzika Kurwa SuperSukcesu', 'vipAmount' => 300],
      ['name' => 'OczytanaKurwa', 'vipAmount' => 500],
      ['name' => 'Wkroczenie w Zawód', 'vipAmount' => 100],
      ['name' => 'photo', 'vipAmount' => 100]
    ];

    public static function getAchievementCompleted(int $userID) {
      return Achievement::whereHas('achievementComplete', function ($query) use ($userID) {
          $query->where('uid', $userID);
      })->get();
    }

    public static function getAchievementAvailable(int $userID): array {
      $achievements = Achievement::whereDoesntHave('achievementComplete', function ($query) use ($userID) {
          $query->where('uid', $userID);
      })->get()->toArray();
      foreach($achievements as $key => $achievement) {
        $index = array_search($achievement['nazwa'], array_column(Achievement::$achievementVipRewards, 'name'));
        if(!is_bool($index)) {
          $achievements[$key]['vip'] = Achievement::$achievementVipRewards[$index]['vipAmount'];
        }else {
          $achievements[$key]['vip'] = $achievement['level'] * 5;
        }
      }
      return $achievements;
    }

}
