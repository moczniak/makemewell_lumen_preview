<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class MemberStatistic extends Model {
    public $timestamps = false;
    protected $table = 'members_stats';

    protected $fillable = ['statID', 'uid', 'work_sec', 'bar_sec', 'orgia_sec', 'hajs',
                           'ciaza', 'syf', 'klienci', 'bar_byl', 'pvp_myself', 'klan_war',
                           'sell_item', 'add_opo', 'pvp'];
    protected $primaryKey = 'statID';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }

    public static function getStatisticByUserID(int $userID) {
      return MemberStatistic::where('uid', $userID)->first();
    }


}
