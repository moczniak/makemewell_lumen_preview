<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class LocationUnlock extends Model {
    public $timestamps = false;
    protected $table = 'latarnia_unlock';

    protected $fillable = ['unlockID', 'mid', 'uid'];
    protected $primaryKey = 'unlockID';





}
