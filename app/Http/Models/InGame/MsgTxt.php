<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class MsgTxt extends Model {
    public $timestamps = false;
    protected $table = 'msg_txt';

    protected $fillable = ['txtID', 'mid','autor','tekst', 'post_date'];
    protected $primaryKey = 'txtID';

    public function msgList() {
      return $this->belongsTo('App\Http\Models\InGame\MsgList', 'mid', 'mid');
    }

}
