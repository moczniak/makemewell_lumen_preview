<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

use App\Http\Models\InGame\MemberStatistic;


class BarPosition extends Model {
    public $timestamps = false;
    protected $table = 'bar_stanowisko';

    protected $fillable = ['positionID', 'nazwa', 'mnoznik', 'czas_h'];
    protected $primaryKey = 'positionID';


    public static function getUserPosition(MemberStatistic $userStat): array {
      $position =  BarPosition::where('czas_h', '<=', $userStat->bar_sec / 60 / 60)->orderBy('czas_h', 'desc')->first();
      $nextPosition = BarPosition::where("czas_h", ">", $userStat->bar_sec / 60 / 60)->orderBy("czas_h", "asc")->first();
      $hourMoney = 7.5 * (1 +($userStat->bar_sec / 60 / 60 / 100)) * $position->mnoznik;
      $timeToNextPosition = $nextPosition->czas_h - ($userStat->bar_sec / 60 / 60);
      return [
        'positionName' => $position->nazwa,
        'hourMoney' => $hourMoney,
        'timeToNextPosition' => $timeToNextPosition
      ];
    }

}
