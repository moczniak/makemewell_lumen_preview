<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class LocationStatistic extends Model {
    public $timestamps = false;
    protected $table = 'latarnia_stat';

    protected $fillable = ['statisticID', 'uid', 'code', 'been', 'time_sec', 'time_upd'];
    protected $primaryKey = 'statisticID';




}
