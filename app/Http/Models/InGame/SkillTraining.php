<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class SkillTraining extends Model {
    public $timestamps = false;
    protected $table = 'skill_kurs';

    protected $fillable = ['skid', 'uid', 'code', 'start_time', '	end_time', 'ilosc', 'cena'];
    protected $primaryKey = 'skid';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }

    public static function calculateSkillCostAndTime(int $prestigeFactor, int $skillLevel, int $trainingPoints) {
      $price = $skillLevel * $prestigeFactor;
      $minutes = $skillLevel * 0.5;
      for ($i = $skillLevel; $i < $skillLevel + $trainingPoints; ++$i) {
        $price += round($i * $prestigeFactor);
        $minutes += round($i * 0.5);
      }
      return [
        'price' => $price,
        'seconds' => $minutes * 60,
        'trainingPoints' => $trainingPoints
      ];
    }

    public static function saveTraining(Member $user, array $skills, array $vipStatus, array $trainingsIDS): Member {
      $cost = 0;
      $setStart = false;
      $activeTraining = SkillTraining::where('uid', $user->uid)->where('start', 1)->first();
      $waitingTraining = SkillTraining::where('uid', $user->uid)->where('start', 0)->get()->toArray();
      if ($vipStatus['big_kurs']) {
        if (count($trainingsIDS) + count($waitingTraining) + (is_null($activeTraining) ? 0 : 1) > 7) {
          throw new \Exception ('Przekroczyłaś limit szkolenia szmato');
        }
      }else {
        if (count($trainingsIDS) + count($waitingTraining) + (is_null($activeTraining) ? 0 : 1) > 3) {
          throw new \Exception ('Przekroczyłaś limit szkolenia szmato');
        }
      }
      $setStart = !is_null($activeTraining) ? 1 : 0;
      $startTime = !is_null($activeTraining) ? $activeTraining->end_time : time();
      if (count($waitingTraining) > 0) {
        $waitingEndTime = array_search(max($waitingTraining), $waitingTraining);
        $startTime = $waitingEndTime['end_time'] > $startTime ? $waitingEndTime['end_time'] : $startTime;
      }
      foreach ($trainingsIDS as $training) {
        $index = array_search($training, array_column($skills, 'sid'));
        $cost += $skills[$index]['price'];
      }

      if ($cost <= $user->hajs) {
        foreach ($trainingsIDS as $training) {
          $index = array_search($training, array_column($skills, 'sid'));
          $newTraining = new SkillTraining();
          $newTraining->uid = $user->uid;
          $newTraining->start = $setStart == 1 ? 0 : 1;
          $newTraining->code = 'sid' . $training;
          $newTraining->start_time = $startTime;
          $newTraining->ilosc = $skills[$index]['trainingPoints'];
          $newTraining->cena = $skills[$index]['price'];
          $newTraining->end_time = $newTraining->start_time + $skills[$index]['seconds'];
          $startTime = $newTraining->end_time;
          $newTraining->save();
          $setStart = $setStart == 1 ? 1 : $newTraining->start == 1 ? 1 : 0;
        }
        $user->hajs -= $cost;
        $user->save();
        return $user;
      }else {
        throw new \Exception ('Nie masz tylu hajsów');
      }
    }

    public static function getActiveTraining(int $userID, array $skills) {
      $activeTraining = SkillTraining::where('uid', $userID)->where('start', 1)->first();
      if (!is_null($activeTraining)) {
        $index = array_search(str_replace('sid', '', $activeTraining['code']), array_column($skills, 'sid'));
        $skills[$index]['price'] = $activeTraining['cena'];
        $skills[$index]['seconds'] = $activeTraining['end_time'] - $activeTraining['start_time'];
        $skills[$index]['level'] = $activeTraining['ilosc'];
        $skills[$index]['trainingID'] = $activeTraining['skid'];
        return $skills[$index];
      }
    }

    public static function getWaitingTrainings(int $userID, array $skills): array {
      $trainings = SkillTraining::where('uid', $userID)->where('start', 0)->get()->toArray();
      $waitingTrainings = [];
      foreach ($trainings as $training) {
        $index = array_search(str_replace('sid', '', $training['code']), array_column($skills, 'sid'));
        $skills[$index]['price'] = $training['cena'];
        $skills[$index]['seconds'] = $training['end_time'] - $training['start_time'];
        $skills[$index]['level'] = $training['ilosc'];
        $skills[$index]['trainingID'] = $training['skid'];
        array_push($waitingTrainings, $skills[$index]);
      }
      return $waitingTrainings;
    }

    public static function deleteTraining(Member $user, int $trainingID): Member {
      $training = SkillTraining::where('uid', $user->uid)->where('start', 0)->where('skid', $trainingID)->first();
      $user->hajs += ($training->cena * 0.9);
      $user->save();
      $training->delete();
      return $user;
    }

    public static function updateTrainings(int $userID) {
      $activeTraining = SkillTraining::where('uid', $userID)->where('start', 1)->first();
      if (!is_null($activeTraining)) {
        if ($activeTraining->end_time <= time()) {
          MemberSkill::where('uid', $userID)->increment($activeTraining->code, $activeTraining->ilosc);
          $activeTraining->delete();
          SkillTraining::where('uid', $userID)->orderBy('start_time', 'asc')->take(1)->update(['start' => 1]);
          SkillTraining::updateTrainings($userID);
        }
      }
    }

}
