<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class AchievementComplete extends Model {
    public $timestamps = false;
    protected $table = 'osiagniecia_stat';

    protected $fillable = ['completeID', 'osid', 'uid'];
    protected $primaryKey = 'completeID';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }

    public function Achievement() {
      return $this->belongsTo('App\Http\Models\InGame\Achievement', 'osid', 'osid');
    }


    public static function updateCompletedAchievement(Member $user) {
      if(is_null($user->stat)) {
        $user->load('stat');
      }
      $achievementsAvailable = Achievement::getAchievementAvailable($user->uid);
      $achieveInstance = new AchievementComplete();
      foreach ($achievementsAvailable as $achievement) {
        switch ($achievement['typ']) {
          case 'klient':
            $user = $achieveInstance->checkClient($achievement, $user);
          case 'lat_hour':
            $user = $achieveInstance->checkWhoreHour($achievement, $user);
          case 'bar_hour':
            $user = $achieveInstance->checkBarHour($achievement, $user);
          case 'hajs_all':
            $user = $achieveInstance->checkMoneyAll($achievement, $user);
          case 'pvp':
            $user = $achieveInstance->checkPvP($achievement, $user);
          case 'prestiz':
            $user = $achieveInstance->checkPrestige($achievement, $user);
          case 'ach':
            $user = $achieveInstance->checkSuccessWhore($achievement, $user);

        }
      }
      $user->save();

    }

    public function checkClient(array $achievement, Member $user): Member {
      if($achievement['wym'] <= $user->stat->klienci){
        $this->addNew($achievement['osid'], $user->uid);
        $user->vip += $achievement['vip'];
        $user->vipNotMove += $achievement['vip'];
        Raport::newAchievementRaport($user->uid, $achievement['nazwa'], $achievement['vip']);
      }
      return $user;
    }

    public function checkWhoreHour(array $achievement, Member $user) {
      if($achievement['wym'] <= ($user->stat->work_sec / 60 / 60)){
        $this->addNew($achievement['osid'], $user->uid);
        $user->vip += $achievement['vip'];
        $user->vipNotMove += $achievement['vip'];
        Raport::newAchievementRaport($user->uid, $achievement['nazwa'], $achievement['vip']);
      }
      return $user;
    }

    public function checkBarHour(array $achievement, Member $user) {
      if($achievement['wym'] <= ($user->stat->bar_sec / 60 / 60)){
        $this->addNew($achievement['osid'], $user->uid);
        $user->vip += $achievement['vip'];
        $user->vipNotMove += $achievement['vip'];
        Raport::newAchievementRaport($user->uid, $achievement['nazwa'], $achievement['vip']);
      }
      return $user;
    }

    public function checkMoneyAll(array $achievement, Member $user) {
      if($achievement['wym'] <= $user->stat->hajs){
        $this->addNew($achievement['osid'], $user->uid);
        $user->vip += $achievement['vip'];
        $user->vipNotMove += $achievement['vip'];
        Raport::newAchievementRaport($user->uid, $achievement['nazwa'], $achievement['vip']);
      }
      return $user;
    }

    public function checkPvP(array $achievement, Member $user) {
      if($achievement['wym'] <= $user->stat->pvp_myself){
        $this->addNew($achievement['osid'], $user->uid);
        $user->vip += $achievement['vip'];
        $user->vipNotMove += $achievement['vip'];
        Raport::newAchievementRaport($user->uid, $achievement['nazwa'], $achievement['vip']);
      }
      return $user;
    }

    public function checkPrestige(array $achievement, Member $user) {
      if($achievement['wym'] <= $user->prestiz){
        $this->addNew($achievement['osid'], $user->uid);
        $user->vip += $achievement['vip'];
        $user->vipNotMove += $achievement['vip'];
        Raport::newAchievementRaport($user->uid, $achievement['nazwa'], $achievement['vip']);
      }
      return $user;
    }

    public function checkSuccessWhore(array $achievement, Member $user) {

      return $user;
    }

    private function addNew(int $osid, int $userID) {
      $complete = new AchievementComplete();
      $complete->osid = $osid;
      $complete->uid = $userID;
      $complete->save();
    }


}
