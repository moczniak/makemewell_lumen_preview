<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class MemberItem extends Model {
    public $timestamps = false;
    protected $table = 'members_items';

    protected $fillable = ['mit', 'uid', 'iid', 'status', 'typ1', 'val', 'plus', 'upgVal', 'position'];
    protected $primaryKey = 'mit';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }

}
