<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;
use App\Http\Classes\SocketEmitter;
use Illuminate\Pagination\LengthAwarePaginator;

// TODO: powiadomienia na socketach
class Raport extends Model {
    public $timestamps = false;
    protected $table = 'raports';

    protected $fillable = ['rid', 'uid', 'typ', 'timeget', 'tytul', 'wynik', 'status', 'read', 'folder'];
    protected $primaryKey = 'rid';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }

    public function raportDetail() {
      return $this->belongsTo('App\Http\Models\InGame\RaportDetail', 'rid', 'rid');
    }

    public static function deleteRaport(int $raportID, int $userID) {
      $raport = Raport::where('rid', $raportID)->where('uid', $userID)->with('raportDetail')->first();
      if (!is_null($raport)) {
        if (!is_null($raport->raportDetail)) {
          $raport->raportDetail->delete();
        }
        $raport->delete();
      }else {
        throw new \Exception('Nie masz takiego raportu suczo!');
      }
    }

    public static function getMainRaports(int $userID, int $limit): LengthAwarePaginator {
      return Raport::where('uid', $userID)->where('folder', '')->orderBy('timeget', 'desc')->paginate($limit);
    }
    public static function getArchiveRaports(int $userID, int $limit): LengthAwarePaginator {
      return Raport::where('uid', $userID)->where('folder', 'archiwum')->orderBy('timeget', 'desc')->paginate($limit);
    }

    public static function newBarRaport(int $userID, float $money, int $time, int $paymentID) {
      $raport = new Raport();
      $raport->tytul = 'Praca: Bar';
      $raport->wynik = 'Wypłata';
      $raport->status = 1;
      $raport->typ = 'bar';
      $raport->uid = $userID;
      $raport->timeget = $time;
      $raport->save();

      $raportDetail = new RaportDetail();
      $raportDetail->txt = $paymentID . ';' . $money;
      $raportDetail->rid = $raport->rid;
      $raportDetail->save();
      SocketEmitter::emitNewEvent($userID, 'raports');
    }

    public static function newLocationJobRaport(array $raportData) {
      $raport = new Raport();
      $raport->tytul = $raportData['title'];
      $raport->wynik = $raportData['result'];
      $raport->status = $raportData['status'];
      $raport->typ = 'job';
      $raport->uid = $raportData['userID'];
      $raport->timeGet = $raportData['timestamp'];
      $raport->save();

      $raportDetail = new RaportDetail();
      $raportDetail->txt = $raportData['clientName'] . ';' . $raportData['prestige'] . ';' . $raportData['money'];
      $raportDetail->rid = $raport->rid;
      $raportDetail->save();
      SocketEmitter::emitNewEvent($raportData['userID'], 'raports');
    }

    public static function newBabySellSuccessRaport(int $userID, int $date, float $cost) {
      $raport = new Raport();
      $raport->tytul = 'Co się stało z dzieckiem?';
      $raport->wynik = 'Sprzedaż';
      $raport->status = 1;
      $raport->typ = 'dziecko';
      $raport->uid = $userID;
      $raport->timeGet = $date;
      $raport->save();

      $raportDetail = new RaportDetail();
      $raportDetail->txt = $cost;
      $raportDetail->rid = $raport->rid;
      $raportDetail->save();
      SocketEmitter::emitNewEvent($userID, 'raports');
    }

    public static function newBabySellFailedRaport(int $userID, int $date, float $cost) {
      $raport = new Raport();
      $raport->tytul = 'Co się stało z dzieckiem?';
      $raport->wynik = 'Utrata';
      $raport->status = 0;
      $raport->typ = 'dziecko';
      $raport->uid = $userID;
      $raport->timeGet = $date;
      $raport->save();

      $raportDetail = new RaportDetail();
      $raportDetail->txt = $cost;
      $raportDetail->rid = $raport->rid;
      $raportDetail->save();
      SocketEmitter::emitNewEvent($userID, 'raports');
    }

    public static function newAchievementRaport(int $userID, string $achievementName, int $vipReward) {
      $raport = new Raport();
      $raport->tytul = 'Powiadomienie';
      $raport->wynik = 'O kurwa!';
      $raport->status = 1;
      $raport->typ = 'notify';
      $raport->uid = $userID;
      $raport->timeGet = time();
      $raport->save();

      $raportDetail = new RaportDetail();
      $raportDetail->txt = 'Zdobyłaś osiągniecie: <b>' . $achievementName . '</b> za to otrzymujesz ' . $vipReward . 'Vip';
      $raportDetail->rid = $raport->rid;
      $raportDetail->save();
      SocketEmitter::emitNewEvent($userID, 'raports');
    }

}
