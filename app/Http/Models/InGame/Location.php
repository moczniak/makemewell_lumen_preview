<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\InGame\Member;

class Location extends Model {
    public $timestamps = false;
    protected $table = 'latarnia';

    protected $fillable = ['mid', 'code', 'unlock','nazwa','k_hajs', 'k_prestiz', 'prestizn', 'wsp',
                           'sid1', 'sid2', 'sid3', 'sid4', 'sid5',
                           'opis', 'image_icon', 'specialLong'];
    protected $primaryKey = 'mid';


    public static $othersLocations = [
            [
              'code' => 'pvp',
              'nazwa' => 'Konfrontacja',
              'opis' => 'Zapierdol kurwie głupiej z bani!',
              'image_icon' => 'assets/images/latarnia/pvp.png'
            ],
            [
              'code' => 'bar',
              'nazwa' => 'Bar',
              'opis' => 'Idż pracować do baru!',
              'image_icon' => 'assets/images/latarnia/bar.png'
            ],
            [
              'code' => 'odpoczynek',
              'nazwa' => 'Odpoczynek',
              'opis' => 'Odpocznij by zregenerować swoją chcicę!',
              'image_icon' => 'assets/images/latarnia/odpoczynek.png'
            ]
          ];

    public function locationUnlock() {
      return $this->hasOne('App\Http\Models\InGame\LocationUnlock', 'mid', 'mid');
    }

    public function shedule() {
      return $this->hasOne('App\Http\Models\InGame\Shedule', 'location', 'code');
    }

    public function locationStatistic() {
      return $this->hasOne('App\Http\Models\InGame\LocationStatistic', 'code', 'code');
    }

    public static function getLocations(float $prestige, int $userID): array {
      $locationsUnlocked = [];
      $locationsLocked = [];
      $locationsToBuy = [];

      $shedule = Shedule::where('uid', $userID)->get()->toArray();
      $minPrestizn = Location::select('prestizn')->where('prestizn', '!=', 0)->where('unlock',0)->orderBy('prestizn', 'ASC')->first();
      $locations = Location::where('prestizn', '>=', $minPrestizn->prestizn)->orderBy('prestizn','ASC')->with(['locationUnlock' => function($query) use ($userID){
        $query->where('uid', $userID);
      }])->get();
      if ($prestige < $minPrestizn->prestizn) {
        $locationsUnlocked[] = Location::where('prestizn', $minPrestizn->prestizn)->first();
      }

      $locationsFiltered = $locations->filter(function ($location) use ($shedule) {
          return array_search($location->code, array_column($shedule, 'location')) ===  false;
      });
      foreach ($locationsFiltered as $location) {
        if ($location->unlock == 1) {
          if ($location->locationUnlock) {
            if ($prestige >= $location->prestizn) {
              $locationsUnlocked[] = $location;
            }else {
              $locationsLocked[] = $location;
            }
          }else {
            $locationsToBuy[] = $location;
          }
        }else {
          if ($prestige >= $location->prestizn) {
            $locationsUnlocked[] = $location;
          }else {
            $locationsLocked[] = $location;
          }
        }
      }
      foreach(Location::$othersLocations as $location) {
        if (!array_search($location['code'], array_column($shedule, 'location'))) {
          array_unshift($locationsUnlocked, $location);
        }
      }
      return [
        'locationsLocked' => $locationsLocked,
        'locationsUnlocked' => $locationsUnlocked,
        'locationsToBuy' => $locationsToBuy
      ];
    }


    public static function getLocationsFromShedule(int $userID): array {
      $locationsActivated = [];
      $locationsNoActivated = [];
      $sheduleStart_EndStamp = 0;
      $shedules = Shedule::where('uid', $userID)->orderBy('start', 'desc')->with('locationData')->get();


      $getLocationData = function($shedule) {
        if (!is_null($shedule->locationData)) {
          return $shedule->locationData;
        }else {
          $index = array_search($shedule->location, array_column(Location::$othersLocations, 'code'));
          if ($index) {
            return Location::$othersLocations[$index];
          }
        }
      };

      if (!is_null($shedules)) {
        foreach ($shedules as $shedule) {
          if ($shedule->start === 1) {
            $sheduleStart_EndStamp = $shedule->end_time;
            $locationsActivated[] = $getLocationData($shedule);
          }else {
            $locationsNoActivated[] = $getLocationData($shedule);
          }
        }
      }
      return [
        'locationsActivated' => $locationsActivated,
        'locationsNoActivated' => $locationsNoActivated,
        'activatedEndTime' => $sheduleStart_EndStamp
      ];
    }


    public static function getBestLocationAvailable(Member $user): Location {
      return Location::where('prestizn', '<=', $user->prestiz)->orderBy('prestizn','DESC')->with(['locationUnlock' => function($query) use ($user){
              $query->where('uid', $user->uid);
            }])->first();
    }





}
