<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class RaportDetail extends Model {
    public $timestamps = false;
    protected $table = 'raports_dtl';

    protected $fillable = ['rid', 'txt'];
    protected $primaryKey = 'rid';

    public function raport() {
      return $this->belongsTo('App\Http\Models\InGame\Raport', 'rid', 'rid');
    }



}
