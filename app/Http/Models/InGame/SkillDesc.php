<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\InGame\SkillTraining;

class SkillDesc extends Model {
    public $timestamps = false;
    protected $table = 'skills_desc';

    protected $fillable = ['sid', 'nazwa', 'opis', 'img_path'];
    protected $primaryKey = 'sid';



    public static function getSkills(Member $user): array {
      $prestigeFactor = Member::getPrestigeFactors($user->prestiz);
      $skills = SkillDesc::all()->toArray();
      foreach ($skills as $index => $skill) {
        $skillLevel = $user->skills->{'sid' . $skill['sid']};
        $calculatedData = SkillTraining::calculateSkillCostAndTime($prestigeFactor, $skillLevel, $user->setting->kurs_amount);
        $skills[$index]['level'] = $skillLevel;
        $skills[$index]['price'] = $calculatedData['price'];
        $skills[$index]['seconds'] = $calculatedData['seconds'];
        $skills[$index]['trainingPoints'] = $calculatedData['trainingPoints'];
      }
      return $skills;
    }

}
