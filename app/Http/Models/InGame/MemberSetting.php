<?php
namespace App\Http\Models\InGame;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class MemberSetting extends Model {
    public $timestamps = false;
    protected $table = 'members_settings';

    protected $fillable = ['settingID', 'uid', 'bar_hour', 'bar_paid', 'anal', 'bez_gumy', 'latarnia_minute',
                           'antyplod_hour', '	antyplod_taked', 'antyplod_timeset', 'alfons_mode', 'alfons_stamp', 'kurs_amount',
                           'styl', 'jezyk', 'backgroundLink', 'opis'];
    protected $primaryKey = 'settingID';

    public function member() {
      return $this->belongsTo('App\Http\Models\InGame\Member', 'uid', 'uid');
    }

    public static function getSettingsByUserID(int $userID): MemberSetting {
      return MemberSetting::where('uid', $userID)->first();
    }

    public static function updateSettingsByUserID(int $userID, Request $request) {
      $user = MemberSetting::where('uid', $userID)->first();
      $user->alfons_mode = $request->get('alfonsMode');
      $user->alfons_stamp = time();
      $user->anal = $request->get('assMode');
      $user->bez_gumy = $request->get('rubberMode');
      $user->latarnia_minute = $request->get('jobMinutes');
      $user->antyplod_hour = $request->get('babyHour');
      $user->bar_hour = $request->get('barHour');
      $user->save();
    }

    public static function updateTrainingPoints(int $userID, int $trainingPoints) {
      $user = MemberSetting::where('uid', $userID)->first();
      $user->kurs_amount = $trainingPoints;
      $user->save();
    }
}
