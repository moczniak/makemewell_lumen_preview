<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class BrothelBuildsData extends Model {
    public $timestamps = false;
    protected $table = 'burdel_build';

    protected $fillable = ['buildID', 'brid', 'bud1', 'bud2', 'bud3', 'bud4', 'bud5', 'bud6', 'bud7', 'bud8', 'bud9'];
    protected $primaryKey = 'buildID';

    public function brothel() {
      return $this->belongsTo('App\Http\Models\InGame\Brothel', 'brid', 'brid');
    }

}
