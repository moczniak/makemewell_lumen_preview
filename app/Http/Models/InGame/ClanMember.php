<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class ClanMember extends Model {
    public $timestamps = false;
    protected $table = 'klan_members';

    protected $fillable = ['memberID', 'kid', 'uid', 'perm', 'podatek'];
    protected $primaryKey = 'memberID';

    public function clanBonus() {
      return $this->hasOne('App\Http\Models\InGame\ClanBonus', 'kid', 'kid');
    }

    public function clan() {
      return $this->hasOne('App\Http\Models\InGame\Clan', 'kid', 'kid');
    }

}
