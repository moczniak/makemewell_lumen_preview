<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class BarPayment extends Model {
    public $timestamps = false;
    protected $table = 'bar_wyplata';

    protected $fillable = ['pid', 'uid', 'kwota'];
    protected $primaryKey = 'pid';


    public static function newPayment(int $userID, float $money): int {
      $payment = new BarPayment();
      $payment->uid = $userID;
      $payment->kwota = $money;
      $payment->save();
      return $payment->pid;
    }

}
