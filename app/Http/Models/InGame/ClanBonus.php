<?php
namespace App\Http\Models\InGame;

use Illuminate\Database\Eloquent\Model;

class ClanBonus extends Model {
    public $timestamps = false;
    protected $table = 'klan_bon';

    protected $fillable = ['bon_id', 'kid', 'hajs', 'hajsEnd', 'attB', 'attBEnd', 'deffB', 'deffBEnd', 'konf', 'konfEnd'];
    protected $primaryKey = 'bon_id';

    

}
