<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\InGame\Member;
use App\Http\Models\Main\MemberMain;
use App\Http\Models\Main\MembersForgetPsw;

use App\Http\ValidateRules\MainMemberRules;

use GenTux\Jwt\JwtToken;
use GenTux\Jwt\GetsJwtController;
use GenTux\Jwt\GetsJwtToken;
class MembersController extends Controller {
  use GetsJwtToken;

  public function checkTokenAction() {
    if ($this->jwtToken()->validate()) {
      $token = $this->jwtToken();
      return response()->json(['data'=> ['t1_uid' => $token->payload('context.t1_uid')]]);
    }else {
      return response()->json(['message'=> 'Token nieaktywny'])->setStatusCode(400);
    }
  }

  public function loginAction(Request $request, JwtToken $jwt) {
    $this->validate($request, MainMemberRules::$loginRules, MainMemberRules::$loginMsg);
    try {
      $member = MemberMain::signInUser($request->get('username'), $request->get('password'));
      $token = $this->createTokenFromUser($member, $jwt);
      return response()->json(['data'=> ['authToken'=>$token, 't1_uid'=>$member->t1_uid]]);
    }catch (\Exception $e) {
      return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }
  }
  private function createTokenFromUser($user, $jwt) {
    $payload = ['sub' => $user->uid, 'exp' => time() + 3 * 24 * 60 * 60,
                'context' => [
                    't1_uid'=> $user->t1_uid,
                    'login' => $user->login
                ]];
    $token = $jwt->createToken($payload);
    return $token;
  }



  public function forgetPasswordEmailAction(Request $request) {
    $this->validate($request, MainMemberRules::$forgetPasswordEmailRules, MainMemberRules::$forgetPasswordEmailMsg);
    try {
      $instance = MemberMain::getInstanceByEmail($request->get('email'));
      MembersForgetPsw::clearTokens($instance->uid);
      MemberMain::sendEmailWithLink($instance);
      return response()->json(['data'=> ['status'=>true]]);
    }catch(\Exception $e) {
      return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }
  }

  public function forgetNewPasswordAction(Request $request) {
    $this->validate($request, MainMemberRules::$forgetNewPasswordRules, MainMemberRules::$forgetNewPasswordMsg);
    $instance = MembersForgetPsw::getInstanceByToken($request->get('token'));
    MemberMain::changePasswordByInstance($instance->member, $request->get('password'));
    $instance->delete();
    return response()->json(['data'=> ['status'=>true]]);
  }

  public function registerAction(Request $request) {
    $this->validate($request, MainMemberRules::$registerRules, MainMemberRules::$registerMsg);
    MemberMain::createNewUser($request);
    return response()->json(['data'=> ['status' => true]]);
  }

  public function createWorldAction(Request $request, JwtToken $jwt) {
    // $world = $request->get('world'); //jest tylko jeden swiat
    try {
      $user = Member::createWorld_T1(MemberMain::find($this->jwtPayload()['sub']));
      $token = $this->createTokenFromUser($user, $jwt);
      return response()->json(['data'=> ['t1_uid'=> $user->t1_uid, 'authToken'=> $token]]);
    }catch(\Exception $e) {
      return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }
  }


}
