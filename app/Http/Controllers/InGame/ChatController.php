<?php

namespace App\Http\Controllers\InGame;

use App\Http\Controllers\GameController;
use Illuminate\Http\Request;

use App\Http\Models\InGame\Clan;
use App\Http\Models\InGame\Chat;

use App\Http\Classes\SocketEmitter;

class ChatController extends GameController {

  public function getRoomList() {
    return response()->json(['data'=> Chat::getRoomsList($this->userID)]);
  }

  public function newRoomMessage(Request $request, int $roomID) {
    try {
      Chat::newMessage($this->userID, $this->username, $roomID, $request->get('message'));
      return response()->json(['data'=> true]);
    }catch (\Exception $e) {
      return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }
  }

  public function getRoomPermission($roomID) {
    return response()->json(['data'=> Chat::roomPermission($this->userID, $roomID)]);
  }




}
