<?php

namespace App\Http\Controllers\InGame;

use App\Http\Controllers\GameController;
use Illuminate\Http\Request;

use App\Http\Models\InGame\Vip;

class VipController extends GameController {

  public function getServices() {
    $data = Vip::getServicesByUserID($this->userID);
    return response()->json(['data'=> $data]);
  }


}
