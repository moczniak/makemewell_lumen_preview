<?php

namespace App\Http\Controllers\InGame;

use App\Http\Controllers\GameController;
use Illuminate\Http\Request;

use App\Http\Models\InGame\Location;
use App\Http\Models\InGame\MemberSetting;
use App\Http\Models\InGame\Member;
use App\Http\Models\InGame\Shedule;
use App\Http\Models\InGame\Vip;
use App\Http\Models\InGame\SkillDesc;
use App\Http\Models\InGame\SkillTraining;

use App\Http\Models\InGame\Achievement;
use App\Http\Models\InGame\AchievementComplete;

class HouseController extends GameController {

  public function getLocations() {
    $prestige = Member::getMemberPrestigeByUserID($this->userID);
    $data = Location::getLocations($prestige, $this->userID);
    return response()->json(['data'=> $data]);
  }
  public function getLocationsShedule() {
    $data = Location::getLocationsFromShedule($this->userID);
    return response()->json(['data'=> $data]);
  }

  public function getGameSettings() {
    $data = MemberSetting::getSettingsByUserID($this->userID);
    return response()->json(['data'=> $data]);
  }

  public function updateGameSettings(Request $request) {
    $data = MemberSetting::updateSettingsByUserID($this->userID, $request);
    return response()->json(['data'=> $data]);
  }

  public function updateShedule(Request $request) {
    try {
        if (is_array($request->get('shedule'))) {
          $user = Member::getWithSettings($this->userID);
          $vipServicesStatus = Vip::getServicesByUserID($this->userID);
          $sheduleData = Shedule::saveShedule($user, $vipServicesStatus, $request->get('shedule'));
          return response()->json(['data' => $sheduleData]);
        }else {
          throw new \Exception('Brak kafelek');
        }
    }catch(\Exception $e) {
      return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }
  }

  public function getSkillsUserData() {
    $user = Member::getWithSettingsAndSkills($this->userID);
    $skills = SkillDesc::getSkills($user);
    $data = [
      'waitingTraining' => SkillTraining::getWaitingTrainings($this->userID, $skills),
      'activeTraining' => SkillTraining::getActiveTraining($this->userID, $skills),
      'availableTrainings' => $skills,
      'trainingPoints' => $user->setting->kurs_amount
    ];
    return response()->json(['data'=> $data]);
  }

  public function getAvailableSkills() {
    $user = Member::getWithSettingsAndSkills($this->userID);
    $data = [
      'availableTrainings' => SkillDesc::getSkills($user)
    ];
    return response()->json(['data'=> $data]);
  }

  public function getSkillsData() {
    return response()->json(['data'=> SkillDesc::all()]);
  }

  public function updateTrainingPoints(Request $request) {
    MemberSetting::updateTrainingPoints($this->userID, $request->get('trainingPoints'));
    return response()->json(['data'=> true]);
  }

  public function saveTraining(Request $request) {
    try {
        $user = Member::getWithSettingsAndSkills($this->userID);
        $skills = SkillDesc::getSkills($user);
        $vipServicesStatus = Vip::getServicesByUserID($this->userID);
        $user = SkillTraining::saveTraining($user, $skills, $vipServicesStatus, $request->get('trainings'));
        $data = [
          'userInfo' => Member::prepareInfo($user),
          'waitingTraining' => SkillTraining::getWaitingTrainings($this->userID, $skills),
          'activeTraining' => SkillTraining::getActiveTraining($this->userID, $skills),
        ];
        return response()->json(['data'=> $data]);
    }catch (\Exception $e) {
       return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }
  }

  public function deleteTraining(int $trainingID) {
    $user = Member::getMemberByUserID($this->userID);
    $user = SkillTraining::deleteTraining($user, $trainingID);
    $data = [
      'userInfo' => Member::prepareInfo($user)
    ];
    return response()->json(['data'=> $data]);
  }

  public function getAchievement() {
    $data = [
      'completed' => Achievement::getAchievementCompleted($this->userID),
      'available' => Achievement::getAchievementAvailable($this->userID)
    ];
    return response()->json(['data'=> $data]);
  }

}
