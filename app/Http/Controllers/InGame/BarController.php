<?php

namespace App\Http\Controllers\InGame;

use App\Http\Controllers\GameController;
use Illuminate\Http\Request;

use App\Http\Models\InGame\Member;
use App\Http\Models\InGame\Vip;
use App\Http\Models\InGame\MemberStatistic;
use App\Http\Models\InGame\BarPosition;

use App\Http\Classes\BarSlots;

class BarController extends GameController {

  public function getBarUserInfo() {
    $userStatistic = MemberStatistic::getStatisticByUserID($this->userID);
    return response()->json(['data'=> BarPosition::getUserPosition($userStatistic)]);
  }

  public function getSlotsInfo() {
    $user = Member::getWithStatistic($this->userID);
    $data = [
      'slotsPointsPrices' => Vip::$slotsPoints,
      'slotsPoints' => $user->wpoint,
      'slotsRewards' => BarSlots::getRewards($user),
      'columns' => BarSlots::getStarterColumns()
    ];
    return response()->json(['data' => $data]);
  }

  public function getSlotsPlay(int $multiplier) {
    try {
      $user = Member::getWithStatistic($this->userID);
      $slots = new BarSlots();
      $slots->play($user, $multiplier);
      $user = $slots->getUserInstance();
      $data = [
        'columns' => $slots->getColumns(),
        'reward' => $slots->getReward(),
        'userInfo' => Member::prepareInfo($user),
        'slotsPoints' => $user->wpoint
      ];
      return response()->json(['data' => $data]);
    }catch (\Exception $e) {
      return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }

  }




}
