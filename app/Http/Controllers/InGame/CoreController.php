<?php

namespace App\Http\Controllers\InGame;

use App\Http\Controllers\GameController;
use Illuminate\Http\Request;

use App\Http\Models\InGame\Shedule;
use App\Http\Models\InGame\Member;
use App\Http\Models\InGame\MemberSkill;
use App\Http\Models\InGame\Vip;
use App\Http\Models\InGame\BarPositon;
use App\Http\Models\InGame\BarPayment;
use App\Http\Models\InGame\Raport;
use App\Http\Models\InGame\RaportDetail;
use App\Http\Models\InGame\SkillTraining;
use App\Http\Models\InGame\Brothel;
use App\Http\Models\InGame\BrothelBuildsData;

use App\Http\Models\InGame\Achievement;
use App\Http\Models\InGame\AchievementComplete;

use App\Http\Classes\WhoreJob;
use App\Http\Classes\SocketEmitter;

class CoreController extends GameController {

  public function getUserID() {
    return response()->json(['userID'=> $this->userID]);
  }


  public function updateAccount() {
    SkillTraining::updateTrainings($this->userID);
    $user = Member::getWithSettings($this->userID);
    $vipStatus = Vip::getServicesByUserID($this->userID);
    $shedule = new Shedule();
    $data = [
      'shedule' => $shedule->updateShedule($user, $vipStatus),
      'userInfo' => Member::prepareInfo($user),
      'healthInfo' => Member::prepareHealthInfo($user),
      'vipStatus' => $vipStatus,
      'vipFactors' => Vip::$factors
    ];
    AchievementComplete::updateCompletedAchievement($user);
    return response()->json(['data'=> $data]);
  }

  public function setActiveSheduleUpdateOption(Request $request) {
    try {
      $vipStatus = Vip::getServicesByUserID($this->userID);
      $user = Member::getMemberByUserID($this->userID);
      Shedule::setOptionAndStampForActiveShedule($user, $vipStatus, $request->get('option'));
      return response()->json(['data'=> NULL]);
    }catch (\Exception $e) {
      return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }
  }

  public function test() {
    SocketEmitter::emitNewEvent(1, 'raports');
  }


}
