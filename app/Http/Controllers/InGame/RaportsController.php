<?php

namespace App\Http\Controllers\InGame;

use App\Http\Controllers\GameController;
use Illuminate\Http\Request;

use App\Http\Models\InGame\Raport;

class RaportsController extends GameController {

  public function getMainRaports() {
    $data = Raport::getMainRaports($this->userID, 15);
    return response()->json(['data'=> $data]);
  }
  public function getArchiveRaports() {
    $data = Raport::getArchiveRaports($this->userID, 15);
    return response()->json(['data'=> $data]);
  }

  public function deleteRaport(int $raportID) {
    try {
      Raport::deleteRaport($raportID, $this->userID);
      return response()->json();
    }catch(\Exception $e) {
      return response()->json(['message'=> $e->getMessage()])->setStatusCode(400);
    }
  }


}
