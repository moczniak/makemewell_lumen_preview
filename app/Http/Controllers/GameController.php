<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use GenTux\Jwt\JwtToken;
use GenTux\Jwt\GetsJwtToken;

class GameController extends BaseController {

  use GetsJwtToken;

  protected $userID;
  protected $username;
  protected $userToken;

  public function __construct(JwtToken $jwt, Request $request) {
    $this->userToken = $request->get('token');
    $payload = $this->jwtPayload();
    $this->userID = $payload['context']['t1_uid'];
    $this->username = $payload['context']['login'];
  }


}
