<?php namespace App\Http\Classes;

use App\Http\Models\InGame\Member;
use App\Http\Models\InGame\Shedule;
use App\Http\Models\InGame\Brothel;
use App\Http\Models\InGame\ClanMember;
use App\Http\Models\InGame\ClanBonus;
use App\Http\Models\InGame\LocationStatistic;

class WhoreJob {

  public $raportData;

  private $prestige;
  private $money;
  private $timeSeconds;
  private $machinePoints;
  private $hiddenPoints;

  private $user;
  private $activeShedule;
  private $mainBrother;
  private $brothelsCount = 0;
  private $clan;
  private $clanMembersCount = 0;
  private $vipStatus;

  private $raportStatus;
  private $raportResult;
  private $raportClientName;

  public function __construct(Member $user, array $vipStatus, Shedule $activeShedule) {
    $this->vipStatus = $vipStatus;
    if (is_null($user->setting)) {
      $user->load('setting');
    }
    $user->load('skills');
    $user->load('stat');
    $this->user = $user;

    $activeShedule->load('locationData');
    $activeShedule->locationData->load(['locationStatistic' => function($query) use ($user){
      $query->where('uid', $user->uid);
    }]);
    $this->activeShedule = $activeShedule;

    $this->mainBrothel = Brothel::where('uid', $user->uid)->where('mother', 1)->with('builds')->first();
    $this->brothelsCount = Brothel::where('uid', $user->uid)->count();

    $this->clan = ClanMember::where('uid', $user->uid)->with('clanBonus', 'clan')->first();
    if (!is_null($this->clan)) {
          $this->clanMembersCount = ClanMember::where('kid', $this->clan->kid)->count();
    }
  }

  public function main() {
    $this->loadMachinePoints();
    $this->loadTimeFromDiseases();
    $this->setHiddenPoints();
    $chance = $this->calculateSuccessChance();
    if (random_int(1, 100) <= $chance) {
      $this->winner();
    }else {
      $this->loser();
    }
  }

  private function loadMachinePoints() {
    $this->machinePoints = $this->activeShedule->option * 2;
  }

  private function loadTimeFromDiseases() {
    $this->timeSeconds = $this->activeShedule->option * 8;
    $this->timeSeconds += Member::getLocationPlusMinutes($this->user);
    $this->timeSeconds *= 60;
  }

  private function setHiddenPoints() {
    $this->hiddenPoints = $this->user->setting->alfons_mode == 0 ? 1.5 * $this->activeShedule->option : $this->activeShedule->option;
  }

  private function calculateSuccessChance(): int {
    $skillArray = ['sid1', 'sid2', 'sid3', 'sid4', 'sid5'];
    $chance = 0;
    foreach ($skillArray as $skill) {
      $diffrence = $this->user->skills[$skill] - ($this->activeShedule->locationData[$skill] - 3);
      $diffrence = $diffrence > 4 ? 4 : $diffrence;
      $diffrence = $diffrence < -4 ? -4 : $diffrence;
      $chance += ($diffrence * 5);
    }
    return $chance;
  }

  private function winner() {
    $this->calculateMoney();
    $this->calculatePrestige();

    $this->raportStatus = 1;
    $this->raportResult = 'Zadowolony';
    if ($this->activeShedule->locationData->prestizn == 0) {
      $this->raportClientName = 'Kumpel';
    }else {
      $this->raportClientName = $this->checkLuckyClient();
      if (empty($raportClientName)) {
        $this->raportClientName = $this->getClient();
      }
    }
    $this->checkDiseases();
    $this->user->chcica -= $this->activeShedule->option * 3;
    $this->user->chcica_stamp = $this->activeShedule->stamp;
    if ($this->user->chcica < 0) {
      $this->user->chcica = 0;
    }

    if ($this->user->setting->alfons_mode) {
      $this->money *= 0.65;
    }
    $this->addClanTaxes();
    $this->updateModels();
    $this->prepareRaport();

    // TODO: dawanie dupy questy zrobic
  }

  private function loser() {
    $this->machinePoints = 0;
    $this->hiddenPoints = 0;
    $this->raportResult = 'Niezadowolony';
    $this->raportStatus = 0;

    $this->checkDiseases();
    $this->user->chcica -= $this->activeShedule->option * 8;
    if ($this->user->chcica < 0) {
      $this->user->chcica = 0;
    }
    $this->updateModels();
    $this->prepareRaport();
  }

  private function calculateMoney() {
    $hours = 0;
    $factor = (ceil(($this->user->prestiz / 250)) * 0.01);
    $this->money = 7.5 * ( 1 + (($this->user->stat->work_sec / 60 / 60 / 100) * (2 +$factor) * $this->activeShedule->option));
    if(($this->user->stat->time_sec - $this->user->stat->time_upd ) >= (60 * 60)){
  		$hours = ceil($this->user->stat->time_sec / 60 / 60);
  	}
    if ($hours <= 0) {
      $hours = 0.01;
    }
    $this->money += $hours / 2;

    $moneyDefaultBonus = $this->money;
    if (!is_null($this->clan)) {
      if ($this->clan->clanBonus->hajsEnd >= time()) {
        if ($this->clanMembersCount > 4) {
          if ($this->clan->clanBonus->hajs > 0) {
            $bonusFactor = $this->clan->clanBonus->hajs / 100;
            $moneyDefaultBonus *= ($bonusFactor + 1);
						$moneyDefaultBonus += $bonusFactor;
          }
        }
      }
    }
    if ($this->vipStatus['moreHajsKlient']) {
      $moneyDefaultBonus *= Vip::$factors['moreHajsKlient'];
    }
    $randomBonus = (random_int(1, 5) / 100) + 1;

    $this->money += $moneyDefaultBonus;
    $this->money *= $randomBonus;

    $this->money *= 5;
  }

  private function calculatePrestige() {
    $hours = 0;
    if(($this->user->stat->time_sec - $this->user->stat->time_upd ) >= (60 * 60)){
  		$hours = ceil($this->user->stat->time_sec / 60 / 60);
  	}
    $this->prestige = (3 + ceil($hours / 10)) * $this->activeShedule->option;
  }

  private function checkLuckyClient() {
    $percentFactor = 0;
    $locationFactor = $this->activeShedule->locationData->wsp;
    if ($this->activeShedule->locationData->unlock == 1) {
    	$percentFactor = $locationFactor * 0.75;
    }else {
    	$percentFactor = $locationFactor;
    }
    $chance = 10;

    if (random_int(1,100) <= $chance) {
      if (random_int(1,100) <= 10) {
        $this->money *= ($locationFactor + 2.6);
        $this->prestige *= ($percentFactor + 2.6);
        return  'Jedwabisty macaccz';
      }else if (random_int(1,100) <=30) {
        $this->money *= ($locationFactor + 1.95);
        $this->prestige *= ($percentFactor + 1.95);
        return  'Stary galerian';
      }else {
        $this->money *= ($locationFactor + 1.3);
        $this->prestige *= ($percentFactor + 1.3);
        return  'Wielbiciel kobiecych ciał';
      }
    }
    return '';
  }

  private function getClient() {
    $locationFactor = $this->activeShedule->locationData->wsp;
    if ($this->activeShedule->locationData->unlock == 1) {
    	$percentFactor = $locationFactor * 0.75;
    }else {
    	$percentFactor = $locationFactor;
    }

    $calculate = function($factor) {
      $z = 1;
      $wyn = 0;
      for ($i = 0; $i <= 100; ++$i) {
        $wyn = ($wyn + 80) * $z;
        $z += 0.13;
        if (abs(($z - $factor) / $factor) < 0.00001) {
          break;
        }
      }
      return ceil($wyn);
    };

    if ($calculate($locationFactor + 0.65) <= $this->user->prestiz) {
      $this->money *= ($locationFactor + 0.65);
      $this->prestige *= ($percentFactor + 0.65);
      return  'Analowy rozdzieracz';
    }else if ($calculate($locationFactor + 0.52) <= $this->user->prestiz) {
      $this->money *= ($locationFactor + 0.52);
      $this->prestige *= ($percentFactor + 0.52);
      return 'Zwykly piździarz';
    }else if ($calculate($locationFactor + 0.39) <= $this->user->prestiz) {
      $this->money *= ($locationFactor + 0.39);
      $this->prestige *= ($percentFactor + 0.39);
      return 'Majestatyczny obciagacz';
    }else if ($calculate($locationFactor + 0.26) <= $this->user->prestiz) {
      $this->money *= ($locationFactor + 0.26);
      $this->prestige *= ($percentFactor + 0.26);
      return 'Wielbiciel Lodziar';
    }else if ($calculate($locationFactor + 0.13) <= $this->user->prestiz) {
      $this->money *= ($locationFactor + 0.13);
      $this->prestige *= ($percentFactor + 0.13);
      return 'Lizaczkowy sliniak';
    }else {
      $this->money *= ($locationFactor + 0);
      $this->prestige *= ($percentFactor + 0);
      return 'Ręczny kumpel';
    }
  }

  private function checkDiseases() {
    $this->checkAnalAndRubberBonus();
    $this->checkPregnency();
    $this->checkVenerealDiseases();
    $this->checkAssPain();
  }

  private function checkAnalAndRubberBonus() {
    if (empty($this->user->ciaza)) {
  		if($this->user->setting->bez_gumy == 1){
  			$this->money *= 1.20;
  			$this->prestige *= 1.20;
  		}
  	}
    if ($this->user->setting->anal == 1) {
      $this->money *= 1.20;
      $this->prestige *= 1.20;
    }
    if ($this->user->chcica > 3) {
      $this->money *= 1.35;
      $this->prestige *= 1.35;
    }
  }

  private function checkPregnency() {
    $pregnency = false;
    if(empty($this->user->ciaza)){
      $chanceForPregnency = 25;
      if (random_int(1, 100) <= $chanceForPregnency) {
        if ($this->user->setting->bez_gumy == 0) {
          if (random_int(1, 100) <= 1) {
            $pregnency = true;
          }
        }else {
          if (time() <= ($this->user->setting->antyplod_taked  + (30 * 60))) {
            if (random_int(1, 100) <= 2) {
              $pregnency = true;
            }
          }else {
            if (random_int(1,100) <= 30) {
              $pregnency = true;
            }
          }
        }
      }
      if($pregnency){
        $period = explode(';', $this->user->okres);
        if (time() < $period[0] && time() > $period[1]) {
          $this->user->ciaza = '1;'.(time() + (5 * 24 * 60 * 60));
          $this->user->stat->ciaza += 1;
        }
      }
    }
  }

  private function checkVenerealDiseases() {
    $disease = false;
    if($this->user->setting->bez_gumy == 1 && time() > $this->user->syf){
      $chanceForDisease = 40;
      if (random_int(1, 100) <= $chanceForDisease) {
        if (random_int(1, 100) <= 15) {
          $disease = true;
        }
      }
      if ($disease) {
        $this->user->syf = time() + (3 * 24 * 60 * 60);
        $this->user->stat->syf += 1;
      }
    }
  }

  private function checkAssPain() {
    if ($this->user->setting->anal == 1 && $this->user->dupsko == 0) {
      $chanceForPain = 20;
      if (randOm_int(1, 100) <= $chanceForPain) {
        if (random_int(1,100) <= 5) {
          $this->user->dupsko = time() + (2 * 24 * 60 * 60);
          $this->user->setting->anal = 0;
        }
      }
    }
  }

  private function addClanTaxes() {
    if (!is_null($this->clan)) {
      $taxFactor = $this->clan->clan->podatek / 100;
      $moneyFromTax = $this->money * $taxFactor;
      $this->money -= $moneyFromTax;
      $this->clan->clan->skarbiec += $moneyFromTax;
      $this->clan->podatek += $moneyFromTax;
    }
  }

  private function updateModels() {
    $this->user->setting->save();
    $this->user->stat->save();

    $this->user->wpoint += $this->machinePoints;
    $this->user->hiddenPKT += $this->hiddenPoints;
    $this->user->prestiz += $this->prestige;
    $this->user->hajs += $this->money;
    $this->user->save();

    $this->clan->clan->save();
    $this->clan->save();

    if (!is_null($this->activeShedule->locationData->locationStatistic)) {
      $this->activeShedule->locationData->locationStatistic->time_upd = $this->activeShedule->locationData->locationStatistic->time_sec + $this->timeSeconds;
      $this->activeShedule->locationData->locationStatistic->time_sec += $this->timeSeconds;
      $this->activeShedule->locationData->locationStatistic->save();
    }else {
      $statistic = new LocationStatistic();
      $statistic->uid = $this->user->uid;
      $statistic->code = $this->activeShedule->location;
      $statistic->been = 1;
      $statistic->time_sec = $this->timeSeconds;
      $statistic->save();
    }
    $this->activeShedule->option = 0;
    $this->activeShedule->save();

    Brothel::addCashToBrothels($this->money * 10, [], $this->user->uid);
  }

  private function prepareRaport() {
    $this->raportData = [
      'clientName' => $this->raportClientName,
      'prestige' => $this->prestige,
      'money' => $this->money,

      'userID' => $this->user->uid,
      'timestamp' => $this->activeShedule->stamp,
      'title' => 'Praca: ' . $this->activeShedule->locationData->nazwa,
      'status' => $this->raportStatus,
      'result' => $this->raportResult
    ];
  }



}
