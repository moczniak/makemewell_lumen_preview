<?php namespace App\Http\Classes;

class SocketEmitter {

  public static function emitNewEvent(int $userID, string $event, int $dataID = 0) {
    $dataCurl = [
      'userID' => $userID,
      'data' => [ 'type'=>$event, 'dataID' => $dataID ]
    ];

    $socket = new self();
    $socket->sendData($dataCurl, '/newEvent');
  }

  public static function emitNewChatMessage(array $data) {
    $socket = new self();
    $socket->sendData($data, '/newChatMessage');
  }


  private function sendData(array $data, string $route) {
    $headers= array('Accept: application/json','Content-Type: application/json');

    $ch = curl_init(env('SOCKETHOST') . $route);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_exec($ch);
    curl_close($ch);
  }

}
