<?php namespace App\Http\Classes;

use App\Http\Models\InGame\Member;
use App\Http\Models\InGame\Location;

class BarSlots {

  private $columns = [];
  private $reward = ['type' => '', 'reward' => ''];
  private $chanceForWin = 30;
  private $winnerColumnPosition = NULL;
  private $multiplier = 1;

  private $userInstance;

  public static function getRewards(Member $user): array {
    $rewards = [
      ['name' => 'BębnoPunkty', 'multiplier' => 5, 'rewardType' => 'slotsPoints', 'type' => 'slot1', 'chance' => 100],
      ['name' => 'BębnoPunkty', 'multiplier' => 11, 'rewardType' => 'slotsPoints', 'type' => 'slot4', 'chance' => 85],
      ['name' => 'Hajsik', 'multiplier' => BarSlots::moneyFactor($user), 'rewardType' => 'money', 'type' => 'slot5', 'chance' => 55],
      ['name' => 'Przedmiot', 'multiplier' => 0, 'rewardType' => 'item', 'type' => 'slot6', 'chance' => 20],
      ['name' => 'Sława', 'multiplier' => BarSlots::fameFactor($user), 'rewardType' => 'fame', 'type' => 'slot3', 'chance' => 13],
      ['name' => 'VIP', 'multiplier' => 6, 'rewardType' => 'vip', 'type' => 'slot2', 'chance' => 2],
    ];

    return $rewards;
  }

  public static function moneyFactor(Member $user): int {
    if (is_null($user->stat)) {
      $user->load('stat');
    }
    $bestLocation = Location::getBestLocationAvailable($user);
		$prestigeFactor = (ceil(($user->prestiz / 250)) * 0.01);
		$money = 7.5 * ( 1 + (($user->stat->work_sec / 60 / 60 / 100) * (2 +$prestigeFactor) * 1));
		$money *= ($bestLocation->wsp + 0.65) * 2;
		$money = round($money);
    return $money;
  }

  public static function fameFactor(Member $user) {
    $thresholds = [13500, 10500, 9000, 7500, 6000, 4500, 3000, 1500, 0];
    $fameFactors = [0.5, 1, 1.5, 3, 4.3, 7.5, 10, 15, 20];
    $fameFactor = 0;
  	for($i = 0; $i < count($thresholds); ++$i){
  		if($user->slawa >= $thresholds[$i]){
  			$fameFactor = $fameFactors[$i];
        break;
  		}
  	}
  	return $fameFactor * 0.6;
  }

  public static function getStarterColumns() {
    $slots = new self();
    return $slots->starterColumnsWrapper();
  }

  public function starterColumnsWrapper() {
    $this->randomSlots();
    $this->checkMatch();
    return $this->columns;
  }

  public function getColumns() {
    return $this->columns;
  }
  public function getReward() {
    return $this->reward;
  }
  public function getUserInstance() {
    return $this->userInstance;
  }

  public function play(Member $user, int $multiplier) {
    if ($multiplier > 0) {
      $this->userInstance = $user;
      $this->multiplier = $multiplier;
      if (($this->multiplier * 2) <= $this->userInstance->wpoint) {
        $winChance = random_int(1, 100);
        if ($winChance <= $this->chanceForWin) {
          $this->randomSlots();
          $this->checkMatch();
          $this->setWinner();
        }else {
          $this->randomSlots();
          $this->checkMatch();
        }
        $this->saveUpdate();
      }else {
        throw new \Exception ('Nie masz tylu BębnoPunktów');
      }
    }else {
      throw new \Exception ('Mnożnik nie może być mniejszy niż 1!');
    }

  }

  private function setWinner() {
    $chanceForReward = random_int(1, 100);
    $rewards = array_reverse(BarSlots::getRewards($this->userInstance));
    $givenReward = [];
    foreach ($rewards as $reward) {
      if ($chanceForReward <= $reward['chance']){
        $givenReward = $reward;
        break;
      }
    }
    $columnPosition = random_int(0, 2);
    $this->winnerColumnPosition = $columnPosition;
    $this->columns[0][$columnPosition] = $givenReward['type'];
    $this->columns[1][$columnPosition] = $givenReward['type'];
    $this->columns[2][$columnPosition] = $givenReward['type'];
    $this->checkMatch();
    $this->updateUser($givenReward);
  }

  private function updateUser($reward) {
    $this->reward['type'] = $reward['rewardType'];
    switch ($reward['rewardType']) {
      case 'money':
        $this->userInstance->hajs += ($this->multiplier * $reward['multiplier']);
        $this->reward['reward'] = ($this->multiplier * $reward['multiplier']);
      break;
      case 'slotsPoints':
        $this->userInstance->wpoint += ($this->multiplier * $reward['multiplier']);
        $this->reward['reward'] = ($this->multiplier * $reward['multiplier']);
      break;
      case 'item':
        $this->addItem();
      break;
      case 'vip':
        $this->userInstance->vip += ($this->multiplier * $reward['multiplier']);
        $this->reward['reward'] = ($this->multiplier * $reward['multiplier']);
      break;
      case 'fame':
        $this->userInstance->slawa += ($this->multiplier * $reward['multiplier']);
        $this->reward['reward'] = ($this->multiplier * $reward['multiplier']);
      break;

    }
  }

  private function saveUpdate() {
    $this->userInstance->wpoint -= ($this->multiplier * 2);
    $this->userInstance->save();
  }

  private function addItem() {
    // TODO: zrobic nagrode itemowoa
    $this->reward['type'] = 'item';
    $this->reward['reward'] = 'Brak itemsu!';
  }

  private function randomSlots() {
    $slotsArray = ['slot1', 'slot2', 'slot3', 'slot4', 'slot5', 'slot6'];
    $getArray = function($keys) use ($slotsArray) {
      $tmpArray = [];
      foreach ($keys as $key) {
        $tmpArray[] = $slotsArray[$key];
      }
      return $tmpArray;
    };
    if($this->winnerColumnPosition !== 0 ) {
      $this->columns[0] = $getArray(array_rand($slotsArray, 3));
    }
    if($this->winnerColumnPosition !== 1 ) {
      $this->columns[1] = $getArray(array_rand($slotsArray, 3));
    }
    if($this->winnerColumnPosition !== 2 ) {
      $this->columns[2] = $getArray(array_rand($slotsArray, 3));
    }
  }
  private function checkMatch() {
    $compareResult = false;
    if ($this->columns[0][0] == $this->columns[1][0] &&  $this->columns[1][0] == $this->columns[2][0] && $this->winnerColumnPosition !== 0) {
      $compareResult = true;
    }else if ($this->columns[0][1] == $this->columns[1][1] &&  $this->columns[1][1] == $this->columns[2][1] && $this->winnerColumnPosition !== 1) {
      $compareResult = true;
    }else if ($this->columns[0][2] == $this->columns[1][2] &&  $this->columns[1][2] == $this->columns[2][2] && $this->winnerColumnPosition !== 2) {
      $compareResult = true;

    }else if ($this->columns[0][0] == $this->columns[1][1] &&  $this->columns[1][1] == $this->columns[2][2]) {
      $compareResult = true;
    }else if ($this->columns[0][2] == $this->columns[1][1] &&  $this->columns[1][1] == $this->columns[2][0]) {
      $compareResult = true;
    }
    if($compareResult == true) {
      $this->randomSlots();
      $this->checkMatch();
    }
  }



}
