<?php

$app->group(['prefix' => 'api', 'namespace' => 'App\Http\Controllers\Main'], function () use ($app) {
    $app->get('/', function ()    {
        return 'Hello World';
    });

    $app->post('/login', 'MembersController@loginAction');
    $app->get('/checkToken', 'MembersController@checkTokenAction');
    $app->post('/register', 'MembersController@registerAction');
    $app->post('/forgetPasswordEmail', 'MembersController@forgetPasswordEmailAction');
    $app->post('/forgetNewPassword', 'MembersController@forgetNewPasswordAction');
});

$app->group(['prefix' => 'api', 'namespace' => 'App\Http\Controllers\Main', 'middleware' => ['jwt']], function () use ($app) {
    $app->post('/createWorld', 'MembersController@createWorldAction');
});


$app->group(['prefix' => 'api/inGame', 'namespace' => 'App\Http\Controllers\InGame', 'middleware' => ['myJwtAuth']], function () use ($app) {
    /*MISC */
    $app->get('/userIDFromToken', 'CoreController@getUserID');

    /* HOUSE */
    $app->get('/locations', 'HouseController@getLocations');
    $app->get('/locationsShedule', 'HouseController@getLocationsShedule');
    $app->get('/gameSettings', 'HouseController@getGameSettings');
    $app->put('/gameSettings', 'HouseController@updateGameSettings');
    $app->get('/vipServices', 'VipController@getServices');
    $app->put('/shedule', 'HouseController@updateShedule');
    $app->get('/updateAccount', 'CoreController@updateAccount');
    $app->get('/skillsUserData', 'HouseController@getSkillsUserData');
    $app->get('/availableSkills', 'HouseController@getAvailableSkills');
    $app->get('/skillsData', 'HouseController@getSkillsData');
    $app->put('/trainingPoints', 'HouseController@updateTrainingPoints');
    $app->put('/training', 'HouseController@saveTraining');
    $app->delete('/training/{trainingID}', 'HouseController@deleteTraining');
    $app->get('/achievement', 'HouseController@getAchievement');


    /* RAPORTS */
    $app->get('/mainRaports', 'RaportsController@getMainRaports');
    $app->get('/archiveRaports', 'RaportsController@getArchiveRaports');
    $app->delete('/raports/{raportID}', 'RaportsController@deleteRaport');

    /* CHAT */
    $app->get('/chatRoomList', 'ChatController@getRoomList');
    $app->post('/chatRoomMessage/{roomID}', 'ChatController@newRoomMessage');
    $app->get('/chatRoomPermission/{roomID}', 'ChatController@getRoomPermission');

    /* BAR */
    $app->get('/barUserInfo', 'BarController@getBarUserInfo');
    $app->get('/slotsInfo', 'BarController@getSlotsInfo');
    $app->get('/slotsPlay/{multiplier}', 'BarController@getSlotsPlay');

    /* PANELS */
    $app->put('/activeSheduleUpdateOption', 'CoreController@setActiveSheduleUpdateOption');


    $app->get('/test', 'CoreController@test');
});
