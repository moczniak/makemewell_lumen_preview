<?php

namespace App\Http\ValidateRules;


class MainMemberRules {

  public static $loginRules = [
      'username' => 'required|exists:mmwMain.members,login',
      'password' => 'required'
  ];
  public static $loginMsg = [
    'username.required' => 'Nazwa dziwki wymagana',
    'username.exists' => 'Nie ma takiej dziwki',
    'password.required' => 'Hasło wymagane'
  ];

  public static $forgetPasswordEmailRules = [
      'email' => 'required|email|exists:mmwMain.members,email'
  ];
  public static $forgetPasswordEmailMsg = [
    'email.required' => 'Email wymagany',
    'email.email' => 'Niepoprawny adres email',
    'email.exists' => 'Nie ma takiego adresu email'
  ];

  public static $forgetNewPasswordRules = [
      'token' => 'required|exists:mmwMain.members_forget_psw,hash',
      'password' => 'required|min:5',
      'passwordRepeat' => 'required|same:password'
  ];
  public static $forgetNewPasswordMsg = [
    'token.required' => 'Token wymagany',
    'token.exists' => 'Niepoprawny token',
    'password.required' => 'Hasło wymagane',
    'password.min' => 'Hasło musi zawierać minimum :min znaków',
    'passwordRepeat.required' => 'Hasła muszą się zgadzać',
    'passwordRepeat.same' => 'Hasła muszą się zgadzać'
  ];

  public static $registerRules = [
      'username' => 'required|min:5|max:30|alpha_num|unique:mmwMain.members,login',
      'password' => 'required|min:5',
      'passwordRepeat' => 'required|same:password',
      'email' => 'required|email|unique:mmwMain.members,email',
      'rules' => 'accepted'
  ];
  public static $registerMsg = [
    'username.required' => 'Nazwa dziwki wymagana',
    'username.min' => 'Nazwa użytkownika może zawierać minimum :min znaków',
    'username.max' => 'Nazwa użytkownika może zawierać maksimum :max znaków',
    'username.alpha_num' => 'Nazwa dziwki może zawierać tylko znaki alfanumeryczne',
    'username.unique' => 'Nazwa dziwki zajęta',
    'password.required' => 'Hasło wymagane',
    'password.min' => 'Hasło musi zawierać minimum :min znaków',
    'passwordRepeat.required' => 'Hasła muszą się zgadzać',
    'passwordRepeat.same' => 'Hasła muszą się zgadzać',
    'email.required' => 'Adres email wymagany',
    'email.email' => 'Adres email jest niepoprawny',
    'email.unique' => 'Adres email jest zajęty',
    'rules.accepted' => 'Musisz zaakceptować zasady'
  ];




}
